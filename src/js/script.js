var headElement = document.getElementsByTagName('HEAD')[0];

for (const [paramName, paramValue] of (new URLSearchParams(window.location.search)).entries()) {
    var linkElement = document.createElement('link');
    linkElement.rel = 'stylesheet'; 
    linkElement.href =`css/${paramName}/${paramValue}.css`; 
    headElement.appendChild(linkElement); 
}